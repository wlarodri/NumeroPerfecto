package cl.ubb.numeroperfecto;
import static org.junit.Assert.*;

import org.junit.Test;


public class NumeroPerfectoTest {

	@Test
	public void IngresarNumero6() {
	NumeroPerfecto fb = new NumeroPerfecto();
	String resultado;
	
	resultado = fb.juego(6);
	
	assertEquals(resultado,"6");
	}
	
	@Test 
	public void IngresarNumero28(){
		NumeroPerfecto fb = new NumeroPerfecto();
		String resultado;
		
		resultado = fb.juego(28);
		
		assertEquals(resultado,"28");
		
	}@Test 
	public void IngresarNumero496(){
		NumeroPerfecto fb = new NumeroPerfecto();
		String resultado;
		
		resultado = fb.juego(496);
		
		assertEquals(resultado,"496");
		
	}
	@Test 
	public void IngresarNumero500(){
		NumeroPerfecto fb = new NumeroPerfecto();
		String resultado;
		
		resultado = fb.juego(500);
		
		assertEquals(resultado,"0");
		
	}

}
